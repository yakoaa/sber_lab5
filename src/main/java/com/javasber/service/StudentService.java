package com.javasber.service;

import com.javasber.dao.StudentDAO;
import com.javasber.entity.Student;

import java.util.ArrayList;

public class StudentService {
    StudentDAO studentDAO = new StudentDAO();

    public Student getOne(Integer id) {
        return studentDAO.findOneById(id);
    }

    public ArrayList<Student> getAll() {
        return studentDAO.findAll();
    }

    public void delete(Integer id) {
        studentDAO.delete(id);
    }

    public void update(Student student) {
        studentDAO.update(student);
    }

    public void save(Student student) {
        studentDAO.save(student);
    }
}
