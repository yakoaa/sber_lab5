package com.javasber.controller;

import com.javasber.entity.Student;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import com.javasber.service.StudentService;

import java.util.ArrayList;

@RestController
@RequestMapping("/api/javasber")
public class StudentController {

    StudentService studentService = new StudentService();

    @GetMapping(value = "/students",
                produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getStudents() {
        ArrayList<Student> students = new ArrayList<Student>();
        students = studentService.getAll();
        return new ResponseEntity<ArrayList<Student>>(students, HttpStatus.OK);
    }

    @GetMapping(value = "/student/{id}",
                produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getStudent(@PathVariable Integer id) {
        Student student;

        student = studentService.getOne(id);

        if(student == null){
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<Student>(student, HttpStatus.OK);
    }

    @PutMapping(path = "/student/{id}",
                consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> updateStudent (@PathVariable Integer id, @RequestBody Student student) {
        Student currStudent;
        currStudent = studentService.getOne(id);
        if(currStudent == null){
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        student.setId(currStudent.getId());
        studentService.update(student);
        return new ResponseEntity<Student>(currStudent, HttpStatus.OK);
    }

    @PostMapping(path = "/student",
                produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> createStudent(@RequestBody Student student) {
        studentService.save(student);
        return new ResponseEntity(HttpStatus.CREATED);
    }

    @DeleteMapping(path = "/student/{id}")
    public ResponseEntity<?> deleteStudent(@PathVariable Integer id){
        Student studentForDelete;
        studentForDelete = studentService.getOne(id);
        if(studentForDelete == null)
        {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        studentService.delete(id);
        return new ResponseEntity(HttpStatus.OK);
    }
}
