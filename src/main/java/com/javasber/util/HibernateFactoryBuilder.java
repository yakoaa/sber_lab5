package com.javasber.util;

import com.javasber.entity.Student;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;


public class HibernateFactoryBuilder {
    private static SessionFactory sessionFactory;
    public HibernateFactoryBuilder() {
    }

    public SessionFactory getSessionFactory() {
        if (sessionFactory == null){
            try {
                Configuration configuration = new Configuration().configure();
                configuration.addAnnotatedClass(Student.class);

                StandardServiceRegistryBuilder standardServiceRegistryBuilder = new StandardServiceRegistryBuilder().applySettings(configuration.getProperties());

                sessionFactory = configuration.buildSessionFactory(standardServiceRegistryBuilder.build());
            } catch (RuntimeException e){
                throw new RuntimeException("Во время получения новой сессии возникла ошибка: " + e.getMessage());
            }
        }
        return sessionFactory;
    }
}
