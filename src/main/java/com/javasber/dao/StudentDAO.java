package com.javasber.dao;

import com.javasber.entity.Student;
import org.hibernate.Session;
import org.hibernate.Transaction;
import com.javasber.util.HibernateFactoryBuilder;

import javax.persistence.Query;
import java.util.ArrayList;

public class StudentDAO {

    HibernateFactoryBuilder hibernateFactoryBuilder = new HibernateFactoryBuilder();

    public Student findOneById(Integer id) {

        Student student;

        Session session =  hibernateFactoryBuilder.getSessionFactory().openSession();
        Query query = session.createQuery("from Student where id =: studId");
        query.setParameter("studId", id);
        student = (Student) query.getSingleResult();
        session.close();

        return student;
    }

    public ArrayList<Student> findAll(){
        ArrayList<Student> students = new ArrayList<Student>();

        Session session = hibernateFactoryBuilder.getSessionFactory().openSession();
        students = (ArrayList<Student>) session.createQuery("From Student").list();
        session.close();
        return students;
    }

    public void save(Student student) {
        Session session = hibernateFactoryBuilder.getSessionFactory().openSession();
        Transaction tr = session.beginTransaction();

        session.save(student);

        tr.commit();
        session.close();
    }

    public void update(Student student) {
        Session session = hibernateFactoryBuilder.getSessionFactory().openSession();
        Transaction tr = session.beginTransaction();

        Query query = session.createQuery("Update Student set name =: studName, grp =: studGroup where id =: studId");
        query.setParameter("studName", student.getName());
        query.setParameter("studId", student.getId());
        query.setParameter("studGroup", student.getGrp());
        query.executeUpdate();

        tr.commit();
        session.close();
    }

    public void delete(Integer id) {
        Session session = hibernateFactoryBuilder.getSessionFactory().openSession();
        Transaction tr = session.beginTransaction();

        Query query = session.createQuery("Delete Student where id = : studId");
        query.setParameter("studId", id);
        query.executeUpdate();

        tr.commit();

        session.close();
    }
}
