package com.javasber.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.javasber.entity.Student;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = StudentController.class)
public class StudentControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private StudentController studentController;

    @Test
    public void getStudentsTest_ok() throws Exception {
        mockMvc.perform(get("/api/javasber/students")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void getStudentTenTest_ok() throws Exception {
        mockMvc.perform(get("/api/javasber/student/10")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))
                .andExpect(jsonPath("$.name").value("Vasya"))
                .andExpect(jsonPath("$.grp").value("M18-512"));
    }

    @Test
    public void addStudentTest_created() throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        Student student = new Student();
        student.setName("Alex");
        student.setGrp("M18-502");
        mockMvc.perform(post("/api/javasber/student/")
                .accept(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(student)))
                .andExpect(status().isCreated());
    }
}